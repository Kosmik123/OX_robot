
class Field(object):

    field_values = []

    def __init__(self,code=0):
        #print("start")
        self.field_values = [" "," "," "," "," "," "," "," "," "]
        return


    def get_cell(self,nr):
        return self.field_values[nr]

    def set_cell(self,cell,value,secure=True):
        if cell>8:
            print("Za duża wartość!")
            return 1
        #print("Ustawiam komorke " + str(cell) + " na wartość " + str(value))
        if value == 0:
            self.field_values[cell] = " "
        elif value == 1:
            if secure and self.get_cell(cell) != " ":
                print("Pole zajęte")
                return 1
            else:
                self.field_values[cell] = "X"
        elif value == 2:
            if secure and self.get_cell(cell) != " ":
                print("Pole zajęte")
                return 1
            else:
                self.field_values[cell] = "O"

        return 0

    # zmienia kod w plansze, przyjmuje liczby od 0 do 19682

    def load_from_code(self,code):
        for i in range(9):
            spare = code % 3
            #print("Reszta to: " + str(spare))
            self.set_cell(i,spare,False)
            code -= spare
            code /= 3
        return code

    def clear(self):
        self.load_from_code(0)
        return

    def export_code(self):
        code = 0
        for i in range(8,-1,-1):
            cell = self.field_values[i]
            if cell == " ":
                code += 0
            elif cell == "X":
                code += 1
            elif cell == "O":
                code += 2
            code *= 3
        code /= 3
        return code

    def check_winner(self):
        crosses = [0,0,0,0,0,0,0,0,0]
        circles = [0,0,0,0,0,0,0,0,0]
        win_cells = [[1,1,1,0,0,0,0,0,0],[0,0,0,1,1,1,0,0,0],[0,0,0,0,0,0,1,1,1],[1,0,0,1,0,0,1,0,0],[0,1,0,0,1,0,0,1,0],[0,0,1,0,0,1,0,0,1],[1,0,0,0,1,0,0,0,1],[0,0,1,0,1,0,1,0,0]]
        for i in range(9):
            if self.field_values[i] == "X":
                crosses[i] = 1
            elif self.field_values[i] == "O":
                circles[i] = 1

        for chance in range(8):
            sumX = 0
            sumO = 0
            for i in range(9):
                sumX += (win_cells[chance][i] * crosses[i])
                sumO += (win_cells[chance][i] * circles[i])
            #print("Czy ktos wygra?")
            #print(sumX)
            #print(sumO)
            if sumX>=3:
               # print("Wygrywa krzyżyk (gracz)")
                return 1
            elif sumO>=3:
                #print("Wygrywa kółko (robot)")
                return -1
            #print("Nie")

        return 0