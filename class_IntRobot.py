

import os
from class_Field import Field
from random import randint

class IntRobot:

    free_cells = []

    my_field = Field()

    my_sign = 2

    def __init__(self):

        #print("OX-DESTRUCTOR CREATED")

        return

    #wybór najlepszego (lub dobrego) ruchu
    def choose_move(self,situation,robots_sign):

        self.my_sign = robots_sign
        self.free_cells = []
        good_moves = []
        normal_moves = []
        self.my_field.load_from_code(situation)


        #sprawdzenie które pola są wolne
        for i in range(9):
            if self.my_field.get_cell(i) == " ":
                self.free_cells.append(i)
        #print(self.free_cells)

        #szukanie ruchów dzięki którym IntRobot wygra
        for n in self.free_cells:
            self.my_field.load_from_code(situation)
            self.my_field.set_cell(n,self.my_sign)
            win = self.my_field.check_winner()

            # -1 wygrywa kółko, 1 wygrywa krzyżyk
            if win == 1 and self.my_sign == 1:
                good_moves.append(n)
            elif win == -1 and self.my_sign == 2:
                good_moves.append(n)

        # szukanie ruchów dzięki którym przeciwnik nie wygra
        if len(good_moves) == 0:
            for n in self.free_cells:
                self.my_field.load_from_code(situation)
                if self.my_sign == 2: enemy_sign = 1
                if self.my_sign == 1: enemy_sign = 2
                self.my_field.set_cell(n, enemy_sign)
                win = self.my_field.check_winner()

                # -1 wygrywa kółko, 1 wygrywa krzyżyk
                if win == 1 and self.my_sign == 2:
                    good_moves.append(n)
                elif win == -1 and self.my_sign == 1:
                    good_moves.append(n)
                else:
                    normal_moves.append(n)

        #print(good_moves)
        #print(normal_moves)

        # losowanie ruchu spomiędzy ruchów dobrych, jeśli brak to z normalnych
        if len(good_moves) != 0:
            x = randint(0,len(good_moves)-1)
            move = good_moves[x]
        else:
            x = randint(0,len(normal_moves)-1)
            move = normal_moves[x]

        return move



    def what_i_play(self,sign):
        self.my_sign = sign