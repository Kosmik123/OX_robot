
import os
from class_Field import Field
from class_Robot import Robot
from class_IntRobot import IntRobot
from random import randint
#from win32api import GetKeyState
#from win32con import VK_NUMLOCK

def draw_field(field):
    print("Plansza wygląda tak:")
    print("\t+ 0 1 2")
    print("\t0 " + field.get_cell(0) + "|" + field.get_cell(1) + "|" + field.get_cell(2))
    print("\t3 " + field.get_cell(3) + "|" + field.get_cell(4) + "|" + field.get_cell(5))
    print("\t6 " + field.get_cell(6) + "|" + field.get_cell(7) + "|" + field.get_cell(8))
    return

def wait_for_move(sign):
    numers = [9, 6, 7, 8, 3, 4, 5, 0, 1, 2]
    if sign==1:
        sign_name = "krzyżyk"
    elif sign==2:
        sign_name = "kółko"
    num = int(input("Podaj pole, w którym chcesz postawić " + sign_name + "\n"))
    if num > 9:
        return num
    if numpad:
        return numers[num]
    return num

#====================================================  MAIN  ===========================================================

plan = Field(0)
robot = Robot()
robot1 = Robot()
numpad = True
robot2 = IntRobot()

# pętla główna
while True:
    draw_field(plan)
    print("Co chcesz robic? \n0 - Wyczyść komórkę\n1 - Postaw krzyżyk\n2 - Postaw kółko\n3 - Wczytaj z kodu\n4 - Export kodu\n5 - Graj"
    "\n6 - Pokaż wpis pamięci\n7 - Robot vs Robot\n8 - Graj z IntRobotem\n9 - Robot vs IntRobot\n10 - Tryb: ",end='')
    print ("numpad" if numpad else "cyfry")
    num = int(input("11 - Koniec\n"))
    if num < 3 and num >= 0:
        if num > 0: pre_word = "W k"
        if num == 0:
            pre_word = "K"
            post_word = "wyczyścić?  "
        if num == 1: post_word = "wstawić krzyżyk?  "
        if num == 2: post_word = "wstawić kółko?  "
        num2 = int(input(pre_word + "tórą komórkę " + post_word))
        plan.set_cell(num2,num,False)
    elif num == 3:
        num2 = int(input("Podaj kod do wczytania?  "))
        plan.load_from_code(num2)
    elif num == 4:
        print(plan.export_code())
    elif num == 5:
        plan.clear()
        now_moving = randint(0,8)
        your_sign = randint(1,6)
        total_moves = 0
        while total_moves<9:
            pre_state = plan.export_code()
            if (now_moving+total_moves)%2 == 1:
                print("Ruch gracza")
                wrong_move = 1
                while wrong_move == 1:
                    move = wait_for_move(your_sign%2+1)
                    wrong_move = plan.set_cell(move,your_sign%2+1)
                robot.remember_move(move,pre_state,1)

            elif (now_moving+total_moves)%2 == 0:
                print("Teraz robot")
                move = robot.choose_move(pre_state)
                print("Robot robi ruch " + str(move))
                plan.set_cell(move,(your_sign+1)%2+1)
                robot.remember_move(move,pre_state,0)

            total_moves += 1
            if plan.check_winner() != 0:
                break
            draw_field(plan)

        if plan.check_winner() == 0:
            print("Remis :|")

        elif plan.check_winner() == -1:
            print("Wygrywa kółko ")

        elif plan.check_winner() == 1:
            print("Wygrywa krzyżyk ")

        if plan.check_winner() * (-1)**(your_sign%2) == 1:
            print("czyli gracz")
        elif plan.check_winner() * (-1)**(your_sign%2) == -1:
            print("czyli robot")

        print("Robot zapamiętał:")
        robot.consolide_memory(plan.check_winner() * (-1)**(your_sign%2),total_moves)


    elif num == 6:
        num2 = int(input("Podaj kod dla jakiej sytuacji. "))
        print(robot.work_memory[num2])

    elif num == 7:
        num2 = int(input("Ile razy?  "))
        for i in range(num2):

            print(i)

            plan.clear()
            total_moves = 0
            while (total_moves)<9:
                #print(total_moves)
                pre_state = plan.export_code()
                if (total_moves+1)%2 == 0:
                    #print("Teraz robot")
                    move = robot.choose_move(pre_state)
                    #print("Robot robi ruch " + str(move))
                    plan.set_cell(move, 2)
                    robot.remember_move(move,pre_state,0)

                elif (total_moves+1)%2 == 1:
                    #print("Teraz robot1")
                    move = robot1.choose_move(pre_state)
                    #print("Robot1 robi ruch " + str(move))
                    plan.set_cell(move, 1)
                    robot.remember_move(move, pre_state, 1)
                total_moves += 1
                if plan.check_winner() != 0:
                    break

            #if plan.check_winner() == 0:
                #print("Remis :|")

            #elif plan.check_winner() == -1:
                #print("Robot wygrywa ")

            #elif plan.check_winner() == 1:
                #print("Robot1 wygrywa  ")
            #print("Robot zapamiętał:")
            #draw_field(plan)
            robot.consolide_memory(2*plan.check_winner(),total_moves)
            #robot1.work_memory = robot.work_memory

    elif num == 8:
        plan.clear()
        total_moves = 0
        now_moving = randint(0, 9)
        your_sign = randint(1, 6)
        while total_moves<9:
            pre_state = plan.export_code()
            if (now_moving+total_moves)%2 == 1:
                print("Ruch gracza")
                wrong_move = 1
                while wrong_move == 1:
                    move = wait_for_move(your_sign%2+1)
                    wrong_move = plan.set_cell(move,your_sign%2+1)

            elif (now_moving+total_moves)%2 == 0:
                print("Teraz robot")
                move = robot2.choose_move(plan.export_code(),(your_sign+1)%2+1)
                print("Robot robi ruch " + str(move))
                plan.set_cell(move,(your_sign+1)%2+1)

            draw_field(plan)
            total_moves += 1
            if plan.check_winner() != 0:
                break
        draw_field(plan)

        if plan.check_winner() == 0:
            print("Remis :|")

        elif plan.check_winner() == -1:
            print("Wygrywa kółko ")

        elif plan.check_winner() == 1:
            print("Wygrywa krzyżyk ")

    elif num == 9:
        num2 = int(input("Ile razy?  "))
        for i in range(num2):
            print(i)
            plan.clear()
            total_moves = 0
            now_moving = i
            your_sign = randint(1, 6)

            while total_moves<9:

                pre_state = plan.export_code()
                if (now_moving+total_moves)%2 == 0:
                    #print("Teraz robot")
                    move = robot.choose_move(pre_state)
                    #print("Robot robi ruch " + str(move))
                    plan.set_cell(move, (your_sign)%2+1)
                    robot.remember_move(move, pre_state, 0)

                elif (now_moving+total_moves)%2 == 1:
                    #print("Teraz IntRobot")
                    move = robot2.choose_move(plan.export_code(),(your_sign+1)%2+1)
                    #print("IntRobot robi ruch " + str(move))
                    plan.set_cell(move,(your_sign+1)%2+1)
                    robot.remember_move(move, pre_state, 1)

                total_moves += 1
                #draw_field(plan)
                if plan.check_winner() != 0:
                    break
            #draw_field(plan)

            #if plan.check_winner() == 0:
                #print("Remis :|")

            #elif plan.check_winner() == -1:
                #print("Wygrywa kółko ")

            #elif plan.check_winner() == 1:
                #print("Wygrywa krzyżyk ")

            #print("Robot zapamiętał:")
            robot.consolide_memory(plan.check_winner() * (-1) ** ((your_sign+1)% 2), total_moves)


    elif num == 10:
        numpad = not numpad
        print("Zmień tryb wpisywania na numpad" if numpad else "Zmień tryb wpisywania na cyfry")


    elif num == 11:
        exit()