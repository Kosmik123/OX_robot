
import os
from class_Field import Field
from random import randint


class Robot:
    work_memory = []

    my_moves = []
    enemy_moves = []

    start_probability = 96
    min_prob = 1
    max_prob = 1024

    def __init__(self,from_file=True):

        self.zero_memory()
        if from_file:
            self.read_experience()

        return

    #zerowanie pamięci roboczej na 96 i środkowych na 128 oraz zajętych pól na 0
    def zero_memory(self):
        tempole = Field()

        #ustawienie wszystkich komórek na 96
        self.work_memory = [[self.start_probability for x in range(9)] for y in range(19683)]

        #ustawienie środków
        #for i in range(len(self.work_memory)):
            #self.work_memory[i][4] = 128

        #ustawienie zer na komórkach zajętych
        for code in range(len(self.work_memory)):
            tempole.load_from_code(code)
            for el in range(len(self.work_memory[code])):
                if tempole.get_cell(el) != " ":
                    self.work_memory[code][el] = 0

        return

    #wczytywanie danych z pliku do work_memory, jeśli brak pliku to save_memory
    def read_experience(self):

        if os.path.isfile('./exp.txt'):
            file_obj = open("exp.txt", "r")
            #print("plik jest - czytam doświadczenie")

            c = file_obj.read(1)
            # dopuki jest co wczytywać z pliku
            while c != "":
                numb_string = ""

                # wczytuje numer sytuacji
                while c != ":":
                    numb_string += c
                    c = file_obj.read(1)


                file_obj.read(1)
                numb = int(numb_string)
                #print("Dla sytuacji " + numb_string)

                # wczytuje prawdopodobienstwa kolejnych ruchów
                for i in range(9):
                    value_string = ""
                    c = file_obj.read(1)
                    while c != "\t" and c != "\n":
                        value_string += c
                        c = file_obj.read(1)
                    value = int(value_string)
                    #print("P ruchu " + str(i) + " wczytuję " + value_string)
                    self.work_memory[numb][i] = value

                c = file_obj.read(1)
                #print("Zakoczono wczytywanie pliku")


            file_obj.close()
            #print("Zamknięto plik")

        else:
            self.save_memory_to_file()
            #print("pliku nie ma - tworze nowy")

        return

    #zapisuje dane z work_memory do pliku, jeśli brak - tworzenie
    def save_memory_to_file(self):

        file_obj = open("exp.txt.temp", "w")

        for i in range(len(self.work_memory)):
            file_obj.write(str(i) + ":")
            for prob in self.work_memory[i]:
                file_obj.write("\t" + str(prob))
            file_obj.write("\n")

        file_obj.close()
        os.remove("exp.txt")
        os.rename("exp.txt.temp","exp.txt")
        return

    #wybór ruchu
    def choose_move(self,situation):
        state = int(situation)
        #print("Mam do czynienia z sytuacją " + str(state))
        omega = 0

        #obliczenie sumy prawdopodobieństw
        for i in range(9):
            omega += self.work_memory[state][i]
        #print("Omega to: " + str(omega))

        #wylosowanie liczby < omega
        if omega != 0:
            los = randint(0,omega-1)
            #print("Wylosowano liczbę " + str(los))
        else:
            print("Błąd się wkradł!")

        maks = 0
        mini = 0

        #wybór ruchu odpowiadającego wylosowanej liczbie
        for k in range(9):
            maks += self.work_memory[state][k]
            if los>=mini and los<maks:
                move = k
                #print( "Liczba " + str(los) + " jest między " + str(mini) + " a " + str(maks) )
            mini += self.work_memory[state][k]
        return move

    #zapisanie ruchu wykonanego przed chwilą (przez gracza lub robota) do odpowiedniej pamięci (krótkotrwałej)
    def remember_move(self,number,state,who):
        if who == 0:
            self.my_moves.append([int(state),number])
        elif who == 1:
            self.enemy_moves.append([int(state),number])
        return

    #pokazanie pamięci krótkotrwałej
    def show_stm(self):
        print("Robotowe ruchy: ", end = '')
        print(self.my_moves)
        print("Gracza ruchy: ", end = '')
        print(self.enemy_moves)
        return

    #konsolidacja pamięci
    def consolide_memory(self,winner,total):

        modifier = (11-total) * winner
        if winner == 0:
            modifier = 0
        #print("Modyfikator: " + str(modifier))

        #zmiana work_memory na podstawie ruchów wroga
        for i in range(len(self.enemy_moves)):
            transmit = 2
            if winner != 0:
                if i == len(self.enemy_moves):
                    transmit = 5
                elif i == len(self.enemy_moves) - 1:
                    transmit = 4
            move = self.enemy_moves[i]
            self.work_memory[move[0]][move[1]] = max([min([self.work_memory[move[0]][move[1]] + modifier * transmit,self.max_prob]),self.min_prob])
            #print("Ustawiam P twojego ruchu " + str(move[1]) + " na " + str(self.work_memory[move[0]][move[1]]))


        #zmiana work_memory na podstawie ruchów swoich
        for i in range(len(self.my_moves)):
            #transmit = i + 5 - len(self.my_moves)
            transmit = 2
            if winner != 0:
                if i == len(self.my_moves):
                    transmit = 5
                elif i == len(self.my_moves)-1:
                    transmit = 4
            move = self.my_moves[i]
            self.work_memory[move[0]][move[1]] = max([min([self.work_memory[move[0]][move[1]] - modifier * transmit,self.max_prob]),self.min_prob])
            #print("Ustawiam P mojego ruchu " + str(move[1]) + " na " + str(self.work_memory[move[0]][move[1]]))

        #zapis do pliku
        self.save_memory_to_file()

        #zerowanie pamięci krótkotrwałej
        self.my_moves = []
        self.enemy_moves = []

        return